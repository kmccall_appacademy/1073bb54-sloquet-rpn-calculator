class RPNCalculator
  # TODO: your code goes here!
  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def plus
    raise("calculator is empty") if @stack.count < 2
    addend_2 = @stack.pop
    addend_1 = @stack.pop
    @stack << (addend_1 + addend_2)
  end

  def value
    @stack[-1]
  end

  def minus
    raise("calculator is empty") if @stack.count < 2
    subtrahend = @stack.pop
    minuend = @stack.pop
    @stack << (minuend - subtrahend)
  end

  def times
    raise("calculator is empty") if @stack.count < 2
    multiplier = @stack.pop
    multiplicand = @stack.pop
    @stack << (multiplicand*multiplier)
  end

  def divide
    raise("calculator is empty") if @stack.count < 2
    divisor = @stack.pop
    dividend = @stack.pop
    @stack << (dividend/divisor.to_f)
  end

  def tokens(str)
    characters = str.split(" ")
    operations = ["/", "*", "+", "-"]
    @stack = characters.map do |chr|
      if operations.include?(chr)
        chr.to_sym
      else
        chr.to_i
      end
    end
  end

  def evaluate(str)
    stack = tokens(str)
    until stack.count == 1
      stack = new_arr(stack)
    end
    stack[0]
  end

  def new_arr(initial_arr)
    rear_arr = []
    until rear_arr[0].class == Fixnum && rear_arr[1].class == Fixnum
      rear_arr.unshift(initial_arr.pop)
    end
    array_to_calculate = rear_arr[0..2]
    rear_arr = rear_arr[3..-1]
    initial_arr + evaluate_sub_arr(array_to_calculate) + rear_arr
  end

  def evaluate_sub_arr(sub_arr)
    if sub_arr[-1] == :/
      return [sub_arr[0]/sub_arr[1].to_f]
    elsif sub_arr[-1] == :+
      return [sub_arr[0] + sub_arr[1]]
    elsif sub_arr[-1] == :-
      return [sub_arr[0] - sub_arr[1]]
    elsif sub_arr[-1] == :*
      return [sub_arr[0] * sub_arr[1]]
    end
  end
end
